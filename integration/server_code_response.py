# -*- coding: utf-8 -*-
import os
import json
import time
from blinker import Blinker
from connector.socket_listener import SocketListener


class ServerCodeResponse:
    _socketListener = None
    _blinker = None

    def __init__(self):
        self._blinker = Blinker()
        self._socketListener = SocketListener(exchange_name='device_finder', routing_key=os.getenv('DEVICE_HASH'))

    def start(self):
        while True:
            try:
                self._socketListener.listen(callback=self.callback)
            except Exception as exception:
                print("Socket listener exception")
                print(exception)
                time.sleep(5)
                continue

    def callback(self, ch, method, properties, body):
        # print(" [x] Device finder received")

        message = json.loads(body)

        if "type" in message:
            if message['type'] == "red_blink":
                print(' [*] RED BLINK')
                self._blinker.run_blinker(self._blinker.red_led_pin, 1)
            elif message['type'] == "green_blink":
                print(' [*] GREEN BLINK')
                self._blinker.run_blinker(self._blinker.green_led_pin, 1)
            elif message['type'] == "blue_start":
                self._blinker.turn_on_pin(self._blinker.blue_led_pin)
            elif message['type'] == "blue_stop":
                self._blinker.turn_off_pin(self._blinker.blue_led_pin)

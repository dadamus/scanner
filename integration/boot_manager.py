#!/usr/bin/python

import os

class BootManager:
    def is_first_boot(self) -> bool:
        print(os.getcwd() + "/fist_boot.lock")
        return not os.path.isfile(os.getcwd() + "/fist_boot.lock")
    
    def register_first_boot(self): 
         with open(os.getcwd() + "/fist_boot.lock", "a+") as file:
            file.seek(0);
            file.write('fist_boot')
            file.close()

    def remove_first_boot_lock(self): 
        if not self.is_first_boot:
            os.remove(os.getcwd() + "/fist_boot.lock")
# -*- coding: utf-8 -*-
import threading
import time
import os


def barcode_scanner() -> str:
    while True:
        try:
            done = False
            hidraw_number = 0
            for x in range(0, 10):
                if os.path.exists('/dev/hidraw' + str(x)):
                    hidraw_number = x

            fp = open('/dev/hidraw' + str(hidraw_number), 'rb')

            # loger scanner connected

            hid = {4: 'a', 5: 'b', 6: 'c', 7: 'd', 8: 'e', 9: 'f', 10: 'g', 11: 'h', 12: 'i', 13: 'j', 14: 'k',
                   15: 'l', 16: 'm', 17: 'n', 18: 'o', 19: 'p', 20: 'q', 21: 'r', 22: 's', 23: 't', 24: 'u',
                   25: 'v', 26: 'w', 27: 'x', 28: 'y', 29: 'z', 30: '1', 31: '2', 32: '3', 33: '4', 34: '5',
                   35: '6', 36: '7', 37: '8', 38: '9', 39: '0', 44: ' ', 45: '-', 46: '=', 47: '[', 48: ']',
                   49: '\\', 51: ';', 52: '\'', 53: '~', 54: ',', 55: '.', 56: '/'}
            ss = ""

            while not done:

                # Get the character from the HID
                buffer = fp.read(12)
                for c in buffer:
                    if c > 0:

                        #  40 is carriage return which signifies
                        #  we are done looking for characters
                        if int(c) == 40:
                            done = True
                            break

                        #  If we are not shifted then use
                        #  the hid characters

                        else:
                            if 56 >= int(c) >= 4:
                                ss += hid[int(c)]
            return ss

        except Exception:
            time.sleep(3)
